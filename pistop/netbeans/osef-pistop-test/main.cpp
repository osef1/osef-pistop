#include "PiStop.h"

int main()
{
    int ret = -1;

    OSEF::PiStop stop(14, 15, 18);

    if (stop.blinkGreen({1, 0}, {1, 0}, {4, 0}))
    {
        ret = 0;
    }

    return ret;
}

