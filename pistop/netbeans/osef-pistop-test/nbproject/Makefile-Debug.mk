#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-DDEBUG -Wall -Wextra -Werror
CXXFLAGS=-DDEBUG -Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../osef-pistop/dist/Debug/GNU-Linux/libosef-pistop.a ../../../osef-gpio/NetBeans/OSEF_GPIO/dist/Debug/GNU-Linux/libosef_gpio.a ../../../osef-gpio/osef-posix/File/NetBeans/OSEF_File/dist/Debug/GNU-Linux/libosef_file.a ../../../osef-gpio/osef-posix/Time/netbeans/osef-time/dist/Debug/GNU-Linux/libosef-time.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef-pistop-test

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef-pistop-test: ../osef-pistop/dist/Debug/GNU-Linux/libosef-pistop.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef-pistop-test: ../../../osef-gpio/NetBeans/OSEF_GPIO/dist/Debug/GNU-Linux/libosef_gpio.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef-pistop-test: ../../../osef-gpio/osef-posix/File/NetBeans/OSEF_File/dist/Debug/GNU-Linux/libosef_file.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef-pistop-test: ../../../osef-gpio/osef-posix/Time/netbeans/osef-time/dist/Debug/GNU-Linux/libosef-time.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef-pistop-test: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef-pistop-test ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../.. -I../../../osef-gpio/SysGPIO -I../../../osef-gpio/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:
	cd ../osef-pistop && ${MAKE}  -f Makefile CONF=Debug
	cd ../../../osef-gpio/NetBeans/OSEF_GPIO && ${MAKE}  -f Makefile CONF=Debug
	cd ../../../osef-gpio/osef-posix/File/NetBeans/OSEF_File && ${MAKE}  -f Makefile CONF=Debug
	cd ../../../osef-gpio/osef-posix/Time/netbeans/osef-time && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../osef-pistop && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../../../osef-gpio/NetBeans/OSEF_GPIO && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../../../osef-gpio/osef-posix/File/NetBeans/OSEF_File && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../../../osef-gpio/osef-posix/Time/netbeans/osef-time && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
