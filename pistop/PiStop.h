#ifndef OSEFPISTOP_H
#define OSEFPISTOP_H

#include "SysOutputGPIO.h"

namespace OSEF
{
    class PiStop
    {
    public:
        PiStop(const uint32_t& g, const uint32_t& y, const uint32_t& r);
        virtual ~PiStop() = default;

        bool setGreen() const {return green.setHigh();}
        bool setOrange() const {return orange.setHigh();}
        bool setRed() const {return red.setHigh();}

        bool resetGreen() const {return green.setLow();}
        bool resetOrange() const {return orange.setLow();}
        bool resetRed() const {return red.setLow();}

        bool blinkGreen(const timespec& ton, const timespec& toff, const timespec& tseq) const;

        PiStop(const PiStop&) = delete;  // copy constructor
        PiStop& operator=(const PiStop&) = delete;  // copy assignment
        PiStop(PiStop&&) = delete;  // move constructor
        PiStop& operator=(PiStop&&) = delete;  // move assignment

    private:
        SysOutputGPIO green;
        SysOutputGPIO orange;
        SysOutputGPIO red;
    };
}  // namespace OSEF

#endif  // OSEFPISTOP_H
