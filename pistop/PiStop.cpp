#include "PiStop.h"
#include "TimeOut.h"

OSEF::PiStop::PiStop(const uint32_t& g, const uint32_t& y, const uint32_t& r)
        :green(g),
        orange(y),
        red(r) {}

bool OSEF::PiStop::blinkGreen(const timespec& ton, const timespec& toff, const timespec& tseq) const
{
    bool ret = false;

    TimeOut onTimer(ton);
    TimeOut offTimer(toff);
    TimeOut sequenceTimer(tseq);

    if (sequenceTimer.start())
    {
        do
        {
            ret = false;

            bool value = false;
            if (green.getValue(value))
            {
                if (value)
                {
                    if (offTimer.start())
                    {
                        if (green.setLow())
                        {
                            ret = offTimer.sleepOff();
                        }
                    }
                }
                else
                {
                    if (onTimer.start())
                    {
                        if (green.setHigh())
                        {
                            ret = onTimer.sleepOff();
                        }
                    }
                }
            }
        }while (ret && not sequenceTimer.isElapsedNow());
    }

    return ret;
}
